/**
 * Задание 2
 * Написать скрипт который наделяет элемент с атрибутом dragAndDrop возможностью перемещаться по экрану
 * по алгоритму Drag and drop.
 **/

/**
 * Алгоритм
 *
 * 1. Отследить событие mousedown на перетаскиваемом элементе
 *
 * 2. При mousemove - задавать элементу новые координаты
 *
 * 3. При mouseup - "отпустить" элемент  (удалить ненужные обработчики)
 *
 * 4. "Запомнить" положение курсора относительно элемента
 *    (подсказка: можно использовать .getBoundingClientRect();)
 * 5. Передвигать элемент в рамках определенного контейнера
 *    (подсказка: необходимо задать минимальное и максимально перемещение по осям X и Y)
 **/



document.addEventListener('mousedown', event => {
    const dragElement = event.target;
    if (dragElement.hasAttribute('dragAndDrop')) {
        dragElement.style.position = 'absolute';
        dragElement.style.zIndex = '1000';


        const shiftX = event.clientX - dragElement.getBoundingClientRect().left; //запомнили то,где курсор расположен по оси Х
        const shiftY = event.clientY - dragElement.getBoundingClientRect().top; //запомнили то,где курсор расположен по оси Y

        const sunW = document.querySelector('.image-container').offsetWidth; //ширина солнышка
        const sunH = document.querySelector('.image-container').offsetHeight //высота солнышка

        const a = event.pageX - shiftX;
        const b = event.pageY - shiftY;

        const minX = document.querySelector('.sun-container').offsetLeft;
        const maxX = document.querySelector('.sun-container').offsetWidth+minX - sunW;
        const minY = document.querySelector('.sun-container').offsetTop;
        const maxY = document.querySelector('.sun-container').offsetHeight+minY-sunH;


        // перемещение элемента
        const onMouseMove = event => {
            moveAt(event.pageX, event.pageY,  dragElement, shiftX, shiftY, minX, maxX,sunW,minY, maxY,sunH)
        }
        // удаление неиспользуемых (больше) обработчиков
        const onMouseUp = () => {
            document.removeEventListener('mousemove', onMouseMove);
            dragElement.removeEventListener('mouseup', onMouseUp);
        }

        document.addEventListener('mousemove', onMouseMove);
        dragElement.addEventListener('mouseup', onMouseUp);

        dragElement.ondragstart = function () {
            return false;
        }
    }
});

function moveAt(pageX, pageY, dragElement, shiftX, shiftY, minX, maxX,sunW,minY, maxY,sunH) {

    const a = pageX - shiftX;
    const b = pageY - shiftY;
    if (a > maxX) {
        a = maxX;
        dragElement.style.left = a + 'px';
        dragElement.style.top = pageY - shiftY + 'px';
    }
    if (a < minX) {
        a = minX;
        dragElement.style.left = a + 'px';
        dragElement.style.top = pageY - shiftY + 'px';
    }
    if (b > maxY) {
        b = maxY;
    dragElement.style.left = pageX - shiftX + 'px';
    dragElement.style.top = b + 'px';
    }
    if (b < minY) {
        b = minY;
    dragElement.style.left = pageX - shiftX + 'px';
    dragElement.style.top = b + 'px';
    }

    dragElement.style.left = pageX - shiftX + 'px';
    dragElement.style.top = pageY - shiftY + 'px';

}


